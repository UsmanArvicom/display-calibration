import tkinter as tk
from tkinter import ttk
from tkinter.filedialog import asksaveasfile
import os

# root window - Main Window
from tkinter.messagebox import showinfo

root = tk.Tk()
root.geometry('250x250')
root.resizable(False, False)
root.title('Arspectra/Arvicom')
R = 0
G = 0
B = 0
L = 0
# root.columnconfigure(0, weight=1)
# root.columnconfigure(1, weight=3)

#Create a ComboBox
ComboBox=ttk.Combobox(root,state='readonly', values=[
                                    "D-Right",
                                    "D-Left",
                                    "Both"])

#ComboBox default value
ComboBox.current(2)

# bind the selected value changes
def combo_event(event):
    """ handle the month changed event """
    showinfo(
        title='Result',
        message=f'You selected {ComboBox.get()}!'
    )

#ComboBox Event
ComboBox.bind('<<ComboboxSelected>>', combo_event)

ComboBox.grid(
    #column=0,
    columnspan=4,
    row=0,
)

#Save button
btn = ttk.Button(root, text = 'Save', command = lambda : save_button())


# slider's current values
current_value = tk.DoubleVar()
current_value1= tk.DoubleVar()
current_value2= tk.DoubleVar()
current_value3= tk.DoubleVar()
# End slider's current values

#Setting the PATH of adb shell is too important.
#cmd = '/home/developer/platform-tools/adb shell i2cset -fy 0x05 0x1d 0x54 0x32 0x00 0x32 0x00 0x32 0x00 i'
#os.system(cmd)

def RGBtoSHELL(R,G,B):
    global D_Left
    global D_Right
    if(ComboBox.get()=="D-Left"):
        cmd = 'adb shell i2cset -fy 0x05 0x1d 0x54 ' + hex(R) + ' 0x00 ' + hex(G) + ' 0x00 ' + hex(
                B) + ' 0x00 i';
        D_Left      = 'L '+hex(R)+' '+hex(G)+' '+hex(B)+'\n'
        os.system(cmd)
    elif(ComboBox.get()=="D-Right"):
        cmd = 'adb shell i2cset -fy 0x05 0x1b 0x54 ' + hex(R) + ' 0x00 ' + hex(G) + ' 0x00 ' + hex(
                B) + ' 0x00 i';
        D_Right     = 'R '+hex(R)+' '+hex(G)+' '+hex(B)
        os.system(cmd)
    else:
        D_Left   = 'L '+hex(R)+' '+hex(G)+' '+hex(B)+'\n'
        D_Right  = 'R '+hex(R)+' '+hex(G)+' '+hex(B)
        cmd = 'adb shell i2cset -fy 0x05 0x1d 0x54 ' + hex(R) + ' 0x00 ' + hex(G) + ' 0x00 ' + hex(
                B) + ' 0x00 i';
        os.system(cmd)
        cmd = 'adb shell i2cset -fy 0x05 0x1b 0x54 ' + hex(R) + ' 0x00 ' + hex(G) + ' 0x00 ' + hex(
                B) + ' 0x00 i';
        os.system(cmd)
    
def LtoSHELL(L):
    cmd = 'adb shell i2cset -fy 0x05 0x1d 0x22 ' + hex(L) + ' i';
    os.system(cmd)
    cmd = 'adb shell i2cset -fy 0x05 0x1b 0x22 ' + hex(L) + ' i';
    os.system(cmd)

#Save Button defination
def save_button():
    Display_Calibration_values = D_Left+D_Right
    cmd = 'adb shell i2cset -fy 0x05 0x1d 0x54 ' + hex(R) + ' 0x00 ' + hex(G) + ' 0x00 ' + hex(
        B) + ' 0x00 i';
    with open("callibration.txt", 'w') as outputFile:
            outputFile.write(Display_Calibration_values)

# Slider events
def get_current_value():
    R = int(current_value.get())
    B = int(current_value2.get())
    G = int(current_value1.get())
    RGBtoSHELL(R, G, B)
    return '{: .2f}'.format(current_value.get())


def slider_changed(event):
    value_label.configure(text=get_current_value())


def get_current_value1():
    R = int(current_value.get())
    B = int(current_value2.get())
    G = int(current_value1.get())
    RGBtoSHELL(R, G, B)
    return '{: .2f}'.format(current_value1.get())


def slider1_changed(event):
    value_label1.configure(text=get_current_value1())


def get_current_value2():
    R = int(current_value.get())
    B = int(current_value2.get())
    G = int(current_value1.get())
    RGBtoSHELL(R, G, B)
    return '{: .2f}'.format(current_value2.get())


def slider2_changed(event):
    value_label2.configure(text=get_current_value2())

def get_current_value3():
    L = int(current_value3.get())
    LtoSHELL(L)
    return '{: .2f}'.format(current_value3.get())


def slider3_changed(event):
    value_label3.configure(text=get_current_value3())
# End Slider Events

# label for the slider
#     slider_label = ttk.Label(
#         root,
#         text='Slider:'
#     )
#
#     slider_label.grid(
#         column=0,
#         row=0,
#         sticky='w'
#     )

#  slider, root is parent, start from 0, goto 255, orient vertical,
#  Variable gets the current_value, command is executed on slider_changed event
slider = ttk.Scale(
    root,
    from_=0,
    to=255,
    orient='vertical',  # horizontal
    command=slider_changed,
    variable=current_value
)

#  slider, root is parent, start from 0, goto 255, orient vertical,
#  Variable gets the current_value, command is executed on slider_changed event
slider1 = ttk.Scale(
    root,
    from_=0,
    to=255,
    orient='vertical',  # horizontal
    command=slider1_changed,
    variable=current_value1
)

#  slider, root is parent, start from 0, goto 255, orient vertical,
#  Variable gets the current_value, command is executed on slider_changed event
slider2 = ttk.Scale(
    root,
    from_=0,
    to=255,
    orient='vertical',  # horizontal
    command=slider2_changed,
    variable=current_value2
)

slider3 = ttk.Scale(
    root,
    from_=0,
    to=255,
    orient='vertical',  # horizontal
    command=slider3_changed,
    variable=current_value3
)
# Slider placement in window
slider.grid(
    column=0,
    row=1,
)

slider1.grid(
    column=1,
    row=1,
)

slider2.grid(
    column=2,
    row=1,
)

slider3.grid(
    column=3,
    row=1,
)

# End slider placement in window

#Button placed in the grid
btn.grid(
    row=1,
    column=5,
    )

# Start current value label's
current_value_label = ttk.Label(
    root,
    text='R:'
)
current_value_label.grid(
    row=2,
    column=0,
    ipadx=10,
    ipady=10
)

current_value1_label = ttk.Label(
    root,
    text='G:'
)
current_value1_label.grid(
    row=2,
    column=1,
    ipadx=10,
    ipady=10
)

current_value2_label = ttk.Label(
    root,
    text='B:'
)
current_value2_label.grid(
    row=2,
    column=2,
    ipadx=10,
    ipady=10
)

current_value3_label = ttk.Label(
    root,
    text='L:'
)
current_value3_label.grid(
    row=2,
    column=3,
    ipadx=10,
    ipady=10
)
# End current value label's

# Start value label's
value_label = ttk.Label(
    root,
    text=get_current_value()
)
value_label.grid(
    row=2,
    column=0,
)

value_label1 = ttk.Label(
    root,
    text=get_current_value1()
)
value_label1.grid(
    row=2,
    column=1,
)

value_label2 = ttk.Label(
    root,
    text=get_current_value2()
)
value_label2.grid(
    row=2,
    column=2,
)

value_label3 = ttk.Label(
    root,
    text=get_current_value3()
)
value_label3.grid(
    row=2,
    column=3,
)
# End value label's

root.mainloop()
